console.log("hello world");


// [SECTION] Functions
	// functions in JavaScript are ine/blocks of codes that tell our device/application to perform a specific task when called/invoked.
	// it prevents repeating lines/blocks of codes taht perform the same task/function.

	// Function Declarations
		// function statement is the definition of a function .

	/*
		Syntax:
			function functionName(){
				code block (statement)
			}
	
		- function keyword - used to define a javascript function.
		- functionName - name of the functio,which will be used to call/invoked the function.
		- funtion block({}) - indicates the function body.

	*/


	function printName (){
		console.log("My name is John");
	}

	// Function Invocation
		//This run/execute the coe block insidethe function.
	printName();

	declaredFunction(); // we cannot invoked a function that we have not declared/defined yet.

//[SECTION] Function Declaration vs Function Expressions
	function declaredFunction(){
		console.log("hello world from declaredFunction()")
	}



	let variableFunction = function(){
			console.log("Hello Again!");
	}
	variableFunction();


	let funcExpression = function FuncName(){
		console.log("Hello from the other side.");
	}

	funcExpression();

	declaredFunction = function(){
		console.log("updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function(){
		console.log("")
	
	}



	const constantFunc = function(){
		console.log("Initialized with const")
	}

	constantFunc();

// [SECTION] Function Scoping
	/*
		Scope isa accessibility (visibility) of variables
		
		JavaScript Variables ha 3 types of scope:
		1. global scope
		2.r
		3.
	*/

	let globalVar = "Mr. WorldWide";

	// Local Scope
	// console.log(localVar);

	{
		// var localVar = "Armando Perez"
		let localVar = "Armando Perez"
	}

	console.log(globalVar);
	// console.log(localVar);

	// Function Scope
		// Each function creates a new scope.
		//variables defin

	function showName(){
		var functionVar = "joe";
		const functionConst = "john";
		let functionLet = "joey";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
		
	}

	showName();

		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);

	function myNewFunction(){
		let name = "Jane";

		function nestedFunc(){
			let nestedName = "John";
			console.log(name);
		}
		
		// console.log(nestedName);
		nestedFunc();

	}

	
	myNewFunction();

	// nesteFunc();//result to an error
	//Global Scope Variable

	let globalName = "Alex";

	function myNewFunction2(){
		let nameInside = "Renz";

		console.log(globalName);
		console.log(nameInside);
	}

	myNewFunction2();

	
	// alert("Hello World");

	function showSampleAlert(){
		alert("Hello,user");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed");


	// let name = prompt("Enter your name: ");
	// let age = prompt("Enter your age: ")
	// console.log(typeof age);
	// console.log ("Hello, I am "+name+", I am "+age+" years old.")


	// let sampleNullPrompt = prompt("Do not enter anything");
	// console.log(sampleNullPrompt);

	function printWelcomeMessage(){
		let name = prompt("Enter your name: ");

		console.log("Hello, "+ name +"! Welcome to my page!")
	}

	printWelcomeMessage();

// [SECTION] Function Naming COnventions


	function getCourses(){
		let courses = ["Science 101", "math 101", "English 101"
			];
		console.log(courses);
	}

	getCourses();


	